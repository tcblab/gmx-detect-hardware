This project is a quick hack to extract GROMACS CPUID detection into a
standalone project, so that Docker instances of Copernicus workers can
bundle the resulting binary. Running it permits them to easily find
out what GROMACS SIMD level is supported by the hardware they get
allocated.

Requires
--------
* build essentials
* cmake 2.8.8
* C compiler (gcc 4.9 and icc 15 tested)

Usage
-----

To build and install:

    cmake . -DCMAKE_INSTALL_PREFIX=/path/copernicus/knows
    make && make install

Later, Copernicus workers should run the equivalent of the bash
command:

    simd=$(/path/copernicus/knows/bin/gmx-detect-hardware -simd)

capturing stdout, and use that to either configure a build of GROMACS
by passing the result to

    cmake -DGMX_SIMD=$simd

or choose a pre-installed build of GROMACS that e.g. has $simd as part
of its path prefix.
